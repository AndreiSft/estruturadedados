package br.ucsal.av01;

public class TesteQuestao2 {

	public static void main(String[] args) {
		Questao2 fila = new Questao2();
		fila.inserir(2);
		fila.inserir(3);
		fila.inserir(4);
		fila.inserir(5);
		fila.inserir(2);
		fila.inserir(3);
		fila.inserir(4);
		fila.inserir(5);
		fila.inserir(2);
		fila.inserir(3);
		/* Exemplo com a lista cheia
		fila.inserir(2);
		fila.inserir(3);
		fila.inserir(4);
		fila.inserir(5);
		fila.inserir(2);
		fila.inserir(3);
		fila.inserir(4);
		fila.inserir(5);
		fila.inserir(2);
		fila.inserir(3);
		fila.inserir(3);
		*/

		fila.imprimir();
		fila.remover();
		
		fila.imprimir();
	}

}
