package br.ucsal.av01;

public class Questao1 {
	private NOP inicio = null;
	
	public void push(String valor){
		NOP novo = new NOP();
		novo.valor = valor;
		
		if (inicio == null) {
			inicio = novo;
		} else {
			novo.prox = inicio;
			inicio = novo;
		}
	}
	
	public void print(){
		if (inicio == null) {
			System.out.println("Lista vazia");
		} else {
			NOP aux = inicio;
			while (aux != null) {
				System.out.println(aux.valor + " ");
				aux = aux.prox;
			}
		}
		System.out.println();
	}
}

class NOP {
	String valor;
	NOP prox;
}