package br.ucsal.estrutura.dados.fila;

public class Fila {
	private static final String EMPTY = " Vazia !!!";
	private static Integer tamanho = 0;
	private NOF inicio = null;

	public void inserir(int valor) {
		NOF novo = new NOF();
		novo.valor = valor;

		if (inicio == null) {
			inicio = novo;
		} else {
			NOF aux = inicio;
			while (aux.prox != null) {
				aux = aux.prox;
			}
			aux.prox = novo;
		}
		tamanho++;
		System.out.println("O valor " + novo.valor + " foi adicionado!!");
	}

	public void remover() {
		empty("Lista");

		NOF aux = inicio;
		aux = aux.prox;
		inicio = aux;

		System.out.println("O n�mero " + inicio.valor + " foi removido");
		tamanho--;
	}

	public void imprimir() {
		empty("Lista");

		NOF aux = inicio;
		while (aux != null) {
			System.out.print(aux.valor + " ");
			aux = aux.prox;
		}
		System.out.println();
	}

	public boolean contem(int valor) {
		empty("Lista");
		NOF aux = inicio;
		while (aux != null) {
			if (aux.valor == valor) {
				return true;
			}
			aux = aux.prox;
		}
		return false;
	}

	// 1� M�todo que retorna o comprimento da fila
	public Integer comprimento() {
		return tamanho;
	}

	// 3� Inserir na ordem inversa
	public void ordemInversa() {
		Pilha filaInversa = new Pilha();
		NOF aux = inicio;
		while (aux != null) {
			filaInversa.push(aux.valor);
			aux = aux.prox;
		}
		filaInversa.print();
	}
	
    public void popAndInsert() {
    	
    }
	
	private void empty(String estruturaDado) {
		if (inicio == null) {
			System.out.println(estruturaDado + EMPTY);
		}
	}
	
	public void inserirQuarta(int valor) {
		NOF novo = new NOF();
		novo.valor = valor;

		if (inicio == null) {
			inicio = novo;
		} else {
			NOF aux = inicio;
			while (aux.prox != null) {
				aux = aux.prox;
			}
			aux.prox = novo;
		}
		tamanho++;
	}
	
}

class NOF {
	int valor;
	NOF prox;
}
