package br.ucsal.estrutura.dados.fila;

public class Pilha {
	private static final String EMPTY = " Vazia !!!";
	private NOP topo = null;

	public void push(int valor) {
		NOP novo = new NOP();
		novo.valor = valor;
		if (topo == null) {
			topo = novo;
		} else {
			novo.prox = topo;
			topo = novo;
		}
	}

	public void pop() {
		if (topo.prox != null) {
			topo = topo.prox;
		} else {
			topo = null;
		}
	}

	public void print() {
		NOP aux = topo;
		while (aux != null) {
			System.out.print(aux.valor + " ");
			aux = aux.prox;
		}
		System.out.println();
	}

	private void empty(String estruturaDado) {
		if (topo == null) {
			System.out.println(estruturaDado + EMPTY);
		}
	}
}

class NOP {
	int valor;
	NOP prox;
}
