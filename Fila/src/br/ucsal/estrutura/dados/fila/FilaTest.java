package br.ucsal.estrutura.dados.fila;

public class FilaTest {

	public static void main(String[] args) {
		Fila fila = new Fila();
		fila.inserir(1);
		fila.inserir(2);
		fila.inserir(3);
		fila.imprimir();
		fila.remover();
		fila.imprimir();
		// System.out.println(fila.contem(2));
		tratamentoBoolean(fila, fila.contem(2), 2);
		//1�
		imprimir("O comprimento da fila:", fila.comprimento());

        //3�
		fila.ordemInversa();
		
		//5�
		
	}

	// Implementa��o beta
	private static void tratamentoBoolean(Fila fila, boolean x, int valor) {

		if (fila.contem(valor) == true) {
			System.out.println("N�mero " + valor + " existente.");
		} else {
			System.out.println("N�mero " + valor + " inexistente.");
		}
	}
	
	private static void imprimir(String texto, Object object) {
		System.out.println(texto + " " + object);
	} 

}
