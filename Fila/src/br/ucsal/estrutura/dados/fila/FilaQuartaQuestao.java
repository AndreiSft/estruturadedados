package br.ucsal.estrutura.dados.fila;

public class FilaQuartaQuestao {
	private static final int SIZE = 8;
	private static final String SENHA_NORMAL = "N";
	private static final String SENHA_PREF = "P";

	public static void main(String[] args) {
		int[] normal = { 1, 2, 3, 4, 5, 6 };
		int[] preferencial = { 1, 2 };
		Fila alternada = new Fila();

		executor(normal, preferencial, alternada);

	}

	private static void executor(int[] normal, int[] preferencial, Fila alternada) {
		int seq1 = 0, seq2 = 0, contador = 0, i = 0;

		while (i < SIZE) {
			if (contador < 3) {

				alternada.inserirQuarta(normal[seq1]);
				contador++;
				imprimirSenha(SENHA_NORMAL, normal[seq1]);

			} else if (contador == 3) {

				alternada.inserirQuarta(preferencial[seq2]);
				contador = 0;
				imprimirSenha(SENHA_PREF, preferencial[seq2]);
				seq2++;
				seq1--;

			}
			seq1++;
			i++;
		}
	}

	private static void imprimirSenha(String txt, int valor) {
		System.out.println("SENHA: " + valor + txt);
		System.out.println();
	}
}
