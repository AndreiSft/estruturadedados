package br.ucsal;

public class Pilha {
	int cont = 0;
	NO topo = null;

	public void push(int valor) {
		NO novo = new NO();
		novo.valor = valor;
		if (topo == null) {
			topo = novo;
			cont++;
		} else {
			novo.prox = topo;
			topo = novo;
			cont++;
		}
	}

	public void pop() {
		if (topo.prox != null) {
			topo = topo.prox;
			cont--;
		} else {
			cont = 0;
			topo = null;
		}

	}

	public void imprimir() {

		NO aux = topo;
		while (aux != null) {
			System.out.println(aux.valor);
			aux = aux.prox;
		}
	}

	public void pushEnd(int valor) {

	}

	public Pilha invertePilha() {
		if (topo == null)
			return null;

		Pilha auxPilha = new Pilha();

		while (topo != null) {
			auxPilha.push(topo.valor);
			topo = topo.prox;
		}

		topo = auxPilha.topo;
		return this;
	}

	public boolean pilhasIguais(Pilha outraPilha) {
		if (outraPilha.cont != cont) {
			return false;
		}
		NO aux = topo;
		NO aux2 = outraPilha.topo;
		while (aux != null) {
			if (aux.valor != aux2.valor) {
				return false;
			}
			aux = aux.prox;
			aux2 = aux2.prox;
		}
		return true;
	}

	public void comparador(int p1, int p2) {

		if (p1 > p2) {
			System.out.println("Pilha 1 � maior");
		} else if (p1 == p2) {
			System.out.println("Mesmo tamanho");
		} else {
			System.out.println("Pilha 2 � maior");
		}
	}

}

class NO {

	int valor;
	NO prox;

}
